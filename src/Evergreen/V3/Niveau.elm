module Evergreen.V3.Niveau exposing (..)


type Niveau
    = Solide
    | Intermediaire
    | Fragile
