module Evergreen.V3.Participant exposing (..)

import Evergreen.V3.Instrument
import Evergreen.V3.Niveau


type alias Participant =
    { prenom : String
    , nom : String
    , instrument1 : Evergreen.V3.Instrument.Instrument
    , instrument2 : Maybe Evergreen.V3.Instrument.Instrument
    , niveau : Evergreen.V3.Niveau.Niveau
    }
