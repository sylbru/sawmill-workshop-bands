module Evergreen.V3.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V3.Band
import Evergreen.V3.Instrument
import Evergreen.V3.Niveau
import Evergreen.V3.Participant
import Lamdera
import Url


type DetailsState
    = Open
    | Closed


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , csvContents : String
    , participants : Dict.Dict Int Evergreen.V3.Participant.Participant
    , instrumentFilter : Maybe Evergreen.V3.Instrument.Instrument
    , bands : Maybe (List Evergreen.V3.Band.BandIds)
    , bandCountInput : String
    , generationMethod : Evergreen.V3.Band.GenerationMethod
    , manageSection : DetailsState
    , listSection : DetailsState
    }


type alias BackendModel =
    { participants : Dict.Dict Int Evergreen.V3.Participant.Participant
    , bands : Maybe (List Evergreen.V3.Band.BandIds)
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | UserChangedImportTextarea String
    | ClickedImport
    | ClickedReset
    | ChoosedInstrumentFilter (Maybe Evergreen.V3.Instrument.Instrument) Bool
    | ChangedNiveau Int Evergreen.V3.Niveau.Niveau Bool
    | ClickedGenerateBandDistribution
    | ChangedBandCountInput String
    | ChoosedGenerationMethod Evergreen.V3.Band.GenerationMethod Bool


type ToBackend
    = ImportCsvContents String
    | ChangeNiveau Int Evergreen.V3.Niveau.Niveau
    | ResetParticipants
    | GenerateBandDistribution Int Evergreen.V3.Band.GenerationMethod


type BackendMsg
    = ClientConnected Lamdera.SessionId Lamdera.ClientId
    | GotRandomBands (List Evergreen.V3.Band.BandIds)


type ToFrontend
    = GotParticipants (Dict.Dict Int Evergreen.V3.Participant.Participant)
    | GotBands (List Evergreen.V3.Band.BandIds)
