module Evergreen.V3.Band exposing (..)

import AssocSet


type alias BandIds =
    AssocSet.Set Int


type GenerationMethod
    = TotallyRandom
    | InstrumentFirst
