module Evergreen.V3.Instrument exposing (..)


type Instrument
    = Guitare
    | Violon
    | Harmonica
    | Chant
    | Flatfooting
    | Contrebasse
    | Dobro
    | Mandoline
    | BanjoBluegrass
    | BanjoOldtime
