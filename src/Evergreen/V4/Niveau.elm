module Evergreen.V4.Niveau exposing (..)


type Niveau
    = Solide
    | Intermediaire
    | Fragile
