module Evergreen.V4.Participant exposing (..)

import Evergreen.V4.Instrument
import Evergreen.V4.Niveau


type alias Participant =
    { prenom : String
    , nom : String
    , instrument1 : Evergreen.V4.Instrument.Instrument
    , instrument2 : Maybe Evergreen.V4.Instrument.Instrument
    , niveau : Evergreen.V4.Niveau.Niveau
    }
