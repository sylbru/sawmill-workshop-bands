module Evergreen.V4.Instrument exposing (..)


type Instrument
    = Guitare
    | Violon
    | Harmonica
    | Chant
    | Flatfooting
    | Contrebasse
    | Dobro
    | Mandoline
    | BanjoBluegrass
    | BanjoOldtime
