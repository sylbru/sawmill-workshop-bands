module Evergreen.V4.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V4.Band
import Evergreen.V4.Instrument
import Evergreen.V4.Niveau
import Evergreen.V4.Participant
import Lamdera
import Url


type DetailsState
    = Open
    | Closed


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , csvContents : String
    , participants : Dict.Dict Int Evergreen.V4.Participant.Participant
    , instrumentFilter : Maybe Evergreen.V4.Instrument.Instrument
    , bands : Maybe (List Evergreen.V4.Band.BandIds)
    , bandCountInput : String
    , generationMethod : Evergreen.V4.Band.GenerationMethod
    , manageSection : DetailsState
    , listSection : DetailsState
    }


type alias BackendModel =
    { participants : Dict.Dict Int Evergreen.V4.Participant.Participant
    , bands : Maybe (List Evergreen.V4.Band.BandIds)
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | UserChangedImportTextarea String
    | ClickedImport
    | ClickedReset
    | ChoosedInstrumentFilter (Maybe Evergreen.V4.Instrument.Instrument) Bool
    | ChangedNiveau Int Evergreen.V4.Niveau.Niveau Bool
    | ClickedGenerateBandDistribution
    | ChangedBandCountInput String
    | ChoosedGenerationMethod Evergreen.V4.Band.GenerationMethod Bool


type ToBackend
    = ImportCsvContents String
    | ChangeNiveau Int Evergreen.V4.Niveau.Niveau
    | ResetParticipants
    | GenerateBandDistribution Int Evergreen.V4.Band.GenerationMethod


type BackendMsg
    = ClientConnected Lamdera.SessionId Lamdera.ClientId
    | GotRandomBands (List Evergreen.V4.Band.BandIds)


type ToFrontend
    = GotParticipants (Dict.Dict Int Evergreen.V4.Participant.Participant)
    | GotBands (List Evergreen.V4.Band.BandIds)
    | GotMaybeBands (Maybe (List Evergreen.V4.Band.BandIds))
