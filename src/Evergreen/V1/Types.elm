module Evergreen.V1.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V1.Band
import Evergreen.V1.Instrument
import Evergreen.V1.Niveau
import Evergreen.V1.Participant
import Lamdera
import Url


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , csvContents : String
    , participants : Dict.Dict Int Evergreen.V1.Participant.Participant
    , instrumentFilter : Maybe Evergreen.V1.Instrument.Instrument
    , bands : Maybe (List Evergreen.V1.Band.Band)
    , bandCountInput : String
    }


type alias BackendModel =
    { participants : Dict.Dict Int Evergreen.V1.Participant.Participant
    , bands : Maybe (List Evergreen.V1.Band.Band)
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | UserChangedImportTextarea String
    | ClickedImport
    | ClickedReset
    | ChoosedInstrumentFilter (Maybe Evergreen.V1.Instrument.Instrument) Bool
    | ChangedNiveau Int Evergreen.V1.Niveau.Niveau Bool
    | ClickedGenerateBandDistribution
    | ChangedBandCountInput String


type ToBackend
    = ImportCsvContents String
    | ChangeNiveau Int Evergreen.V1.Niveau.Niveau
    | ResetParticipants
    | GenerateBandDistribution Int


type BackendMsg
    = ClientConnected Lamdera.SessionId Lamdera.ClientId


type ToFrontend
    = GotParticipants (Dict.Dict Int Evergreen.V1.Participant.Participant)
    | GotBands (List Evergreen.V1.Band.Band)
