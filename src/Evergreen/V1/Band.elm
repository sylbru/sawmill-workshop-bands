module Evergreen.V1.Band exposing (..)

import AssocSet
import Evergreen.V1.Participant


type alias Band =
    AssocSet.Set Evergreen.V1.Participant.Participant
