module Evergreen.V1.Niveau exposing (..)


type Niveau
    = Solide
    | Moyen
    | Fragile
