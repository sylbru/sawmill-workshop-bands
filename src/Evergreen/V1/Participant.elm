module Evergreen.V1.Participant exposing (..)

import Evergreen.V1.Instrument
import Evergreen.V1.Niveau


type alias Participant =
    { prenom : String
    , nom : String
    , instrument1 : Evergreen.V1.Instrument.Instrument
    , instrument2 : Maybe Evergreen.V1.Instrument.Instrument
    , niveau : Evergreen.V1.Niveau.Niveau
    }
