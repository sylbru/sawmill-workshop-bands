module Evergreen.V2.Niveau exposing (..)


type Niveau
    = Solide
    | Intermediaire
    | Fragile
