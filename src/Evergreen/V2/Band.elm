module Evergreen.V2.Band exposing (..)

import AssocSet
import Evergreen.V2.Participant


type alias Band =
    AssocSet.Set Evergreen.V2.Participant.Participant


type GenerationMethod
    = TotallyRandom
    | InstrumentFirst
