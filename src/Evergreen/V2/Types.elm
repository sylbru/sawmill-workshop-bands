module Evergreen.V2.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V2.Band
import Evergreen.V2.Instrument
import Evergreen.V2.Niveau
import Evergreen.V2.Participant
import Lamdera
import Url


type DetailsState
    = Open
    | Closed


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , csvContents : String
    , participants : Dict.Dict Int Evergreen.V2.Participant.Participant
    , instrumentFilter : Maybe Evergreen.V2.Instrument.Instrument
    , bands : Maybe (List Evergreen.V2.Band.Band)
    , bandCountInput : String
    , generationMethod : Evergreen.V2.Band.GenerationMethod
    , manageSection : DetailsState
    , listSection : DetailsState
    }


type alias BackendModel =
    { participants : Dict.Dict Int Evergreen.V2.Participant.Participant
    , bands : Maybe (List Evergreen.V2.Band.Band)
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | UserChangedImportTextarea String
    | ClickedImport
    | ClickedReset
    | ChoosedInstrumentFilter (Maybe Evergreen.V2.Instrument.Instrument) Bool
    | ChangedNiveau Int Evergreen.V2.Niveau.Niveau Bool
    | ClickedGenerateBandDistribution
    | ChangedBandCountInput String
    | ChoosedGenerationMethod Evergreen.V2.Band.GenerationMethod Bool


type ToBackend
    = ImportCsvContents String
    | ChangeNiveau Int Evergreen.V2.Niveau.Niveau
    | ResetParticipants
    | GenerateBandDistribution Int Evergreen.V2.Band.GenerationMethod


type BackendMsg
    = ClientConnected Lamdera.SessionId Lamdera.ClientId
    | GotRandomBands (List Evergreen.V2.Band.Band)


type ToFrontend
    = GotParticipants (Dict.Dict Int Evergreen.V2.Participant.Participant)
    | GotBands (List Evergreen.V2.Band.Band)
