module Evergreen.V2.Participant exposing (..)

import Evergreen.V2.Instrument
import Evergreen.V2.Niveau


type alias Participant =
    { prenom : String
    , nom : String
    , instrument1 : Evergreen.V2.Instrument.Instrument
    , instrument2 : Maybe Evergreen.V2.Instrument.Instrument
    , niveau : Evergreen.V2.Niveau.Niveau
    }
