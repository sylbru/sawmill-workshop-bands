module Band exposing (BandIds, BandParticipants, GenerationMethod(..), allGenerationMethods, createBands, generationMethodToString)

import AssocSet as Set exposing (Set)
import Dict exposing (Dict)
import Instrument exposing (Instrument(..))
import List.Extra
import Niveau
import Participant exposing (Participant)
import Random
import Random.List


type alias BandParticipants =
    Set Participant


type alias BandIds =
    Set Int


type GenerationMethod
    = TotallyRandom
    | InstrumentFirst


allGenerationMethods : List GenerationMethod
allGenerationMethods =
    [ InstrumentFirst, TotallyRandom ]


generationMethodToString : GenerationMethod -> String
generationMethodToString generationMethod =
    case generationMethod of
        TotallyRandom ->
            "Complètement aléatoire"

        InstrumentFirst ->
            "Répartition par cours"


hasParticipant : Participant -> BandParticipants -> Bool
hasParticipant participant band =
    Set.member participant band


scoreFromBands : List BandParticipants -> Int
scoreFromBands bands =
    let
        countFragileInBand : BandParticipants -> Int
        countFragileInBand band =
            band
                |> Set.toList
                |> List.filter (\participant -> participant.niveau == Niveau.Fragile)
                |> List.length
    in
    bands
        |> List.map countFragileInBand
        |> List.maximum
        |> Maybe.withDefault 0


createBands : Int -> GenerationMethod -> Dict Int Participant -> Random.Generator (List BandIds)
createBands count method participants =
    (case method of
        TotallyRandom ->
            createBandsRandom count participants

        InstrumentFirst ->
            createBandsInstrumentFirst count participants
                |> Random.list 100
                |> Random.map
                    (\bandsDistributions ->
                        List.Extra.minimumBy scoreFromBands bandsDistributions
                            |> Maybe.withDefault []
                    )
    )
        |> Random.map (List.map (bandParticipantsToBandIds participants))


bandParticipantsToBandIds : Dict Int Participant -> BandParticipants -> BandIds
bandParticipantsToBandIds participants band =
    band
        |> Set.toList
        |> List.filterMap (findParticipantId participants)
        |> Set.fromList


findParticipantId : Dict Int Participant -> Participant -> Maybe Int
findParticipantId participants participant =
    participants
        |> Dict.filter (\k p -> p == participant)
        |> Dict.keys
        |> List.head


createBandsRandom : Int -> Dict k Participant -> Random.Generator (List BandParticipants)
createBandsRandom count participants =
    let
        membersPerBand =
            Dict.size participants // count
    in
    Dict.values participants
        |> Random.List.shuffle
        |> Random.map
            (List.Extra.groupsOf membersPerBand >> List.map Set.fromList)


createBandsInstrumentFirst : Int -> Dict k Participant -> Random.Generator (List BandParticipants)
createBandsInstrumentFirst count participants =
    let
        listInstrumentsFromMostPresentToLeast : List Participant -> List Instrument
        listInstrumentsFromMostPresentToLeast allParticipants =
            let
                countParticipantsForInstrument instrument_ allParticipants_ =
                    allParticipants_
                        |> List.filter (Participant.hasInstrument instrument_)
                        |> List.length
            in
            Instrument.all
                |> List.map (\instrument -> ( countParticipantsForInstrument instrument allParticipants, instrument ))
                |> List.sortBy Tuple.first
                |> List.reverse
                |> List.map Tuple.second

        addParticipantsOfInstrument : List Participant -> Instrument -> { index : Int, bandsInProgress : List BandParticipants } -> { index : Int, bandsInProgress : List BandParticipants }
        addParticipantsOfInstrument participants_ instrument { index, bandsInProgress } =
            let
                -- A participant may have already been added to another band as they may have several instruments
                -- So we need to filter those out first
                isFree : Participant -> Bool
                isFree participant =
                    not <| List.any (hasParticipant participant) bandsInProgress

                participantsOfInstrument : List Participant
                participantsOfInstrument =
                    participants_
                        |> List.filter (Participant.hasInstrument instrument)
                        |> List.filter isFree
            in
            List.foldl
                (\participant acc ->
                    let
                        newBands =
                            List.Extra.updateAt
                                acc.index
                                (\band -> Set.insert participant band)
                                acc.bandsInProgress
                    in
                    { index = acc.index + 1 |> modBy count, bandsInProgress = newBands }
                )
                { index = index, bandsInProgress = bandsInProgress }
                participantsOfInstrument
    in
    participants
        |> Dict.values
        |> Random.List.shuffle
        |> Random.map
            (\shuffledParticipants ->
                listInstrumentsFromMostPresentToLeast shuffledParticipants
                    |> List.foldl
                        (addParticipantsOfInstrument shuffledParticipants)
                        { index = 0, bandsInProgress = List.repeat count Set.empty }
                    |> .bandsInProgress
            )
