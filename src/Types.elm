module Types exposing (..)

import Band exposing (BandIds, BandParticipants, GenerationMethod)
import Browser exposing (UrlRequest)
import Browser.Navigation exposing (Key)
import Dict exposing (Dict)
import Instrument exposing (Instrument)
import Json.Encode
import Lamdera exposing (ClientId, SessionId)
import Niveau exposing (Niveau)
import Participant exposing (Participant)
import Url exposing (Url)


type alias FrontendModel =
    { key : Key
    , csvContents : String
    , participants : Dict Int Participant
    , instrumentFilter : Maybe Instrument
    , bands : Maybe (List BandIds)
    , bandCountInput : String
    , generationMethod : GenerationMethod
    , manageSection : DetailsState
    , listSection : DetailsState
    }


type DetailsState
    = Open
    | Closed


type alias BackendModel =
    { participants : Dict Int Participant
    , bands : Maybe (List BandIds)
    }


type FrontendMsg
    = UrlClicked UrlRequest
    | UrlChanged Url
    | UserChangedImportTextarea String
    | ClickedImport
    | ClickedReset
    | ChoosedInstrumentFilter (Maybe Instrument) Bool
    | ChangedNiveau Int Niveau Bool
    | ClickedGenerateBandDistribution
    | ChangedBandCountInput String
    | ChoosedGenerationMethod GenerationMethod Bool


type ToBackend
    = ImportCsvContents String
    | ChangeNiveau Int Niveau
    | ResetParticipants
    | GenerateBandDistribution Int GenerationMethod


type BackendMsg
    = ClientConnected SessionId ClientId
    | GotRandomBands (List BandIds)


type ToFrontend
    = GotParticipants (Dict Int Participant)
    | GotBands (List BandIds)
    | GotMaybeBands (Maybe (List BandIds))
