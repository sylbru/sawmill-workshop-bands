module Frontend exposing (..)

import AssocSet as Set exposing (Set)
import Band exposing (BandIds, BandParticipants, GenerationMethod(..))
import Browser exposing (UrlRequest(..))
import Browser.Navigation as Nav
import Csv.Decode exposing (FieldNames(..))
import Dict exposing (Dict)
import Html exposing (Attribute, Html)
import Html.Attributes as Attr
import Html.Events as Events
import Instrument exposing (Instrument)
import Json.Encode
import Lamdera
import Niveau
import Participant exposing (Participant)
import Types exposing (..)
import Url


type alias Model =
    FrontendModel


app =
    Lamdera.frontend
        { init = init
        , onUrlRequest = UrlClicked
        , onUrlChange = UrlChanged
        , update = update
        , updateFromBackend = updateFromBackend
        , subscriptions = \m -> Sub.none
        , view = view
        }


init : Url.Url -> Nav.Key -> ( Model, Cmd FrontendMsg )
init url key =
    ( { key = key
      , csvContents = ""
      , participants = Dict.empty
      , instrumentFilter = Nothing
      , bands = Nothing
      , bandCountInput = "5"
      , generationMethod = InstrumentFirst
      , manageSection = Open
      , listSection = Closed
      }
    , Cmd.none
    )


detailsStateToProperty : DetailsState -> Attribute msg
detailsStateToProperty state =
    (case state of
        Open ->
            True

        Closed ->
            False
    )
        |> Json.Encode.bool
        |> Attr.property "open"


update : FrontendMsg -> Model -> ( Model, Cmd FrontendMsg )
update msg model =
    case msg of
        UrlClicked urlRequest ->
            case urlRequest of
                Internal url ->
                    ( model
                    , Nav.pushUrl model.key (Url.toString url)
                    )

                External url ->
                    ( model
                    , Nav.load url
                    )

        UrlChanged url ->
            ( model, Cmd.none )

        UserChangedImportTextarea newValue ->
            case Csv.Decode.decodeCsv Csv.Decode.FieldNamesFromFirstRow Participant.csvDecoder newValue of
                Ok participants ->
                    ( { model
                        | csvContents = newValue
                      }
                    , Cmd.none
                    )

                Err error ->
                    let
                        _ =
                            Debug.log "CSV decoding error" error
                    in
                    ( { model | csvContents = newValue }, Cmd.none )

        ClickedImport ->
            ( { model | csvContents = "" }, Lamdera.sendToBackend (ImportCsvContents model.csvContents) )

        ClickedReset ->
            ( { model | csvContents = "" }, Lamdera.sendToBackend ResetParticipants )

        ChoosedInstrumentFilter maybeInstrument _ ->
            ( { model | instrumentFilter = maybeInstrument }, Cmd.none )

        ChangedNiveau id niveau _ ->
            ( model, Lamdera.sendToBackend (ChangeNiveau id niveau) )

        ClickedGenerateBandDistribution ->
            case String.toInt model.bandCountInput of
                Just bandCount ->
                    ( model, Lamdera.sendToBackend (GenerateBandDistribution bandCount model.generationMethod) )

                Nothing ->
                    ( model, Cmd.none )

        ChangedBandCountInput newCount ->
            ( { model | bandCountInput = newCount }, Cmd.none )

        ChoosedGenerationMethod method _ ->
            ( { model | generationMethod = method }, Cmd.none )


updateFromBackend : ToFrontend -> Model -> ( Model, Cmd FrontendMsg )
updateFromBackend msg model =
    case msg of
        GotParticipants participants ->
            ( { model
                | participants = participants
                , manageSection =
                    if Dict.isEmpty participants then
                        Open

                    else
                        Closed
                , listSection =
                    if Dict.isEmpty participants then
                        Closed

                    else
                        Open
              }
            , Cmd.none
            )

        GotMaybeBands maybeBands ->
            ( { model | bands = maybeBands }, Cmd.none )

        GotBands bands ->
            ( { model | bands = Just bands }, Cmd.none )


view : Model -> Browser.Document FrontendMsg
view model =
    { title = ""
    , body =
        [ Html.main_ []
            [ Html.h1 [] [ Html.text "Sawmill Workshop - Ateliers collectifs" ]
            , Html.h2 [] [ Html.text "Participants" ]
            , Html.details [ detailsStateToProperty model.manageSection ]
                [ Html.summary [] [ Html.text "Gestion des participants" ]
                , Html.textarea
                    [ Events.onInput UserChangedImportTextarea
                    , Attr.placeholder "Copier ici le contenu du tableur au format CSV (séparateur virgule)"
                    , Attr.value model.csvContents
                    ]
                    []
                , Html.fieldset
                    [ Attr.style "display" "flex"
                    , Attr.style "justify-content" "space-between"
                    ]
                    [ case readyToImport model.csvContents of
                        Nothing ->
                            Html.button [ Attr.disabled True ] [ Html.text "Rien à importer pour l’instant" ]

                        Just count ->
                            Html.button
                                [ Events.onClick ClickedImport ]
                                [ Html.text <|
                                    "Importer "
                                        ++ String.fromInt count
                                        ++ " ligne"
                                        ++ (if count > 1 then
                                                "s"

                                            else
                                                ""
                                           )
                                ]
                    , Html.button
                        [ Events.onClick ClickedReset
                        , Attr.class "contrast"
                        , Attr.attribute "data-tooltip" "La liste des participants sera supprimée."
                        ]
                        [ Html.text "⚠ Tout supprimer ⚠" ]
                    ]
                ]
            , Html.hr [] []
            , Html.details [ detailsStateToProperty model.listSection ]
                [ Html.summary [] [ Html.text <| "Voir la liste (" ++ String.fromInt (Dict.size model.participants) ++ ")" ]
                , viewParticipants model.participants model.instrumentFilter
                ]
            , Html.hr [] []
            , Html.h2 [] [ Html.text "Distribution des groupes" ]
            , Html.label
                [ Attr.for "band_count_input" ]
                [ Html.text "Nombre de groupes à générer" ]
            , Html.input
                [ Attr.type_ "number"
                , Attr.min "1"
                , Attr.max "10"
                , Attr.value model.bandCountInput
                , Attr.id "band_count_input"
                , Attr.style "flex-shrink" "2"
                , Events.onInput ChangedBandCountInput
                ]
                []
            , Html.div [ Attr.attribute "role" "group" ]
                [ Html.button [ Events.onClick ClickedGenerateBandDistribution ] [ Html.text "Générer" ]
                ]
            , case model.bands of
                Nothing ->
                    Html.text ""

                Just bands ->
                    viewBands bands model.participants
            ]
        ]
    }


readyToImport : String -> Maybe Int
readyToImport csvContents =
    csvContents
        |> Csv.Decode.decodeCsv FieldNamesFromFirstRow Participant.csvDecoder
        |> Result.map List.length
        |> Result.toMaybe


viewParticipants : Dict Int Participant -> Maybe Instrument -> Html FrontendMsg
viewParticipants participants instrumentFilter =
    let
        radio : Maybe Instrument -> List (Html FrontendMsg)
        radio maybeInstrument =
            let
                label =
                    maybeInstrument
                        |> Maybe.map Instrument.toString
                        |> Maybe.withDefault "Tous instruments"

                htmlId =
                    "instrument_filter_"
                        ++ (label |> String.replace " " "_" |> String.toLower)
            in
            [ Html.span [ Attr.style "white-space" "nowrap", Attr.style "margin-right" "1em" ]
                [ Html.input
                    [ Attr.type_ "radio"
                    , Attr.name "instrument_filter"
                    , Attr.value label
                    , Attr.id htmlId
                    , Attr.checked (maybeInstrument == instrumentFilter)
                    , Events.onCheck (ChoosedInstrumentFilter maybeInstrument)
                    ]
                    []
                , Html.label [ Attr.for htmlId ] [ Html.text label ]
                ]
            , Html.text " "
            ]

        relevantParticipants : Dict Int Participant
        relevantParticipants =
            case instrumentFilter of
                Nothing ->
                    participants

                Just instrument ->
                    Dict.filter
                        (\_ participant ->
                            (participant.instrument1 == instrument)
                                || (participant.instrument2 == Just instrument)
                        )
                        participants

        participantsCount : Int
        participantsCount =
            Dict.size relevantParticipants
    in
    Html.div []
        [ Html.fieldset []
            ((Nothing :: List.map Just Instrument.all)
                |> List.concatMap radio
            )
        , Html.p []
            [ Html.strong []
                [ Html.text <|
                    String.join " "
                        [ String.fromInt (Dict.size relevantParticipants)
                        , if participantsCount > 1 then
                            "participant·e·s"

                          else
                            "participant·e"
                        , "en"
                        , case instrumentFilter of
                            Nothing ->
                                "tout"

                            Just instrument ->
                                Instrument.toString instrument
                        , ":"
                        ]
                ]
            ]
        , Html.ul [] (relevantParticipants |> Dict.toList |> List.map viewParticipant)
        ]


viewParticipant : ( Int, Participant ) -> Html FrontendMsg
viewParticipant ( id, participant ) =
    Html.li []
        [ Html.text <|
            "#"
                ++ String.fromInt id
                ++ " "
                ++ Participant.toString participant
                ++ " ("
                ++ Participant.toInstrumentString participant
                ++ ")"
        , Html.fieldset
            []
            (Niveau.all
                |> List.map
                    (\niveau ->
                        let
                            htmlId =
                                "participant-" ++ String.fromInt id ++ "-" ++ Niveau.toString niveau
                        in
                        [ Html.input
                            [ Attr.type_ "radio"
                            , Events.onCheck (ChangedNiveau id niveau)
                            , Attr.checked (participant.niveau == niveau)
                            , Attr.id <| htmlId
                            ]
                            []
                        , Html.label [ Attr.for htmlId ] [ Html.text (Niveau.toString niveau) ]
                        ]
                    )
                |> List.concat
            )
        ]


viewBands : List BandIds -> Dict Int Participant -> Html FrontendMsg
viewBands bands participants =
    let
        bandIdsToBandParticipants participants_ band =
            band
                |> Set.toList
                |> List.filterMap (\id -> Dict.get id participants_)
                |> Set.fromList
    in
    Html.div []
        [ Html.ol []
            (bands |> List.map (bandIdsToBandParticipants participants >> viewBand))
        ]


scoreFromBands : List BandIds -> Dict Int Participant -> Int
scoreFromBands bands participants =
    let
        countFragileInBand : BandIds -> Int
        countFragileInBand band =
            band
                |> Set.toList
                |> List.filterMap (\id -> Dict.get id participants)
                |> List.filter (\participant -> participant.niveau == Niveau.Fragile)
                |> List.length
    in
    bands
        |> List.map countFragileInBand
        |> List.maximum
        |> Maybe.withDefault 0


viewBand : BandParticipants -> Html FrontendMsg
viewBand band =
    Html.li []
        [ Html.ul []
            (band
                |> Set.toList
                |> List.map
                    (\participant ->
                        Html.li []
                            ([ Html.text (participant.niveau |> Niveau.toEmoji) ]
                                ++ [ Html.text " "
                                   , Html.text (Participant.toString participant)
                                   , Html.text " "
                                   ]
                                ++ Participant.toInstrumentBadges participant
                            )
                    )
            )
        ]



-- Spécification :
-- - On décide combien de groupes on veut
-- - Les apprenants sont assignés aux groupes de manière aléatoire
--   mais en respectant ces conditions :
--   - tous les groupes ont à peu près le même nombre d’apprenants
--   - tous les groupes n’ont pas plus de deux au niveau « fragile »
--   - les groupes ont une répartition « régulière » d’instruments
--       - du chant dans chaque groupe
--       - de la guitare dans chaque groupe ? en fait ça dépend des participants
--       -
