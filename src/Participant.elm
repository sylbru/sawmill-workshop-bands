module Participant exposing (Participant, csvDecoder, hasInstrument, toInstrumentBadges, toInstrumentString, toString, toStringWithAdditionalInfo)

import Csv.Decode
import Html exposing (Html)
import Html.Attributes as Attr
import Instrument exposing (Instrument)
import Niveau exposing (Niveau)


type alias Participant =
    { prenom : String
    , nom : String
    , instrument1 : Instrument
    , instrument2 : Maybe Instrument
    , niveau : Niveau
    }


csvDecoder : Csv.Decode.Decoder Participant
csvDecoder =
    Csv.Decode.into (\prenom nom instrument1 maybeInstrument2 -> { prenom = prenom, nom = nom, instrument1 = instrument1, instrument2 = maybeInstrument2, niveau = Niveau.default })
        |> Csv.Decode.pipeline (Csv.Decode.field "Prénom" Csv.Decode.string)
        |> Csv.Decode.pipeline (Csv.Decode.field "Nom" Csv.Decode.string)
        |> Csv.Decode.pipeline (Csv.Decode.field "Instrument 1" Instrument.decoder)
        |> Csv.Decode.pipeline (Csv.Decode.field "Instrument 2" (Csv.Decode.blank Instrument.decoder))


toStringWithAdditionalInfo : Participant -> String
toStringWithAdditionalInfo participant =
    toString participant
        ++ " "
        ++ Niveau.toEmoji participant.niveau


toString : Participant -> String
toString participant =
    participant.prenom ++ " " ++ String.toUpper participant.nom


toInstrumentString : Participant -> String
toInstrumentString participant =
    Instrument.toString participant.instrument1
        ++ (case participant.instrument2 of
                Just instrument2 ->
                    ", " ++ Instrument.toString instrument2

                Nothing ->
                    ""
           )


toInstrumentBadges : Participant -> List (Html msg)
toInstrumentBadges participant =
    let
        instruments : List Instrument
        instruments =
            [ Just participant.instrument1, participant.instrument2 ]
                |> List.filterMap identity
    in
    List.map Instrument.toBadge instruments


hasInstrument : Instrument -> Participant -> Bool
hasInstrument instrument participant =
    participant.instrument1 == instrument || participant.instrument2 == Just instrument
