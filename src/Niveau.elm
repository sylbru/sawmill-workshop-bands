module Niveau exposing (Niveau(..), all, default, toEmoji, toString)

import Csv.Decode


type Niveau
    = Solide
    | Intermediaire
    | Fragile


toString : Niveau -> String
toString niveau =
    case niveau of
        Fragile ->
            "Fragile"

        Intermediaire ->
            "Intermédiaire"

        Solide ->
            "Solide"


toEmoji : Niveau -> String
toEmoji niveau =
    case niveau of
        Fragile ->
            "🔴"

        Solide ->
            "🟢"

        _ ->
            "⚫"


all : List Niveau
all =
    [ Fragile, Intermediaire, Solide ]


default : Niveau
default =
    Intermediaire
