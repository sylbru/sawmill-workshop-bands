module Backend exposing (..)

import AssocSet as Set exposing (Set)
import Band
import Csv.Decode
import Dict exposing (Dict)
import Html
import Lamdera exposing (ClientId, SessionId)
import Niveau exposing (Niveau)
import Participant exposing (Participant)
import Random
import Types exposing (..)


type alias Model =
    BackendModel


app =
    Lamdera.backend
        { init = init
        , update = update
        , updateFromFrontend = updateFromFrontend
        , subscriptions = \m -> Lamdera.onConnect ClientConnected
        }


init : ( Model, Cmd BackendMsg )
init =
    ( { participants = Dict.empty
      , bands = Nothing
      }
    , Cmd.none
    )


update : BackendMsg -> Model -> ( Model, Cmd BackendMsg )
update msg model =
    case msg of
        ClientConnected sessionId clientId ->
            ( model
            , Cmd.batch
                [ Lamdera.sendToFrontend sessionId (GotParticipants model.participants)
                , Lamdera.sendToFrontend sessionId (GotMaybeBands model.bands)
                ]
            )

        GotRandomBands bands ->
            ( { model | bands = Just bands }, Lamdera.broadcast (GotBands bands) )


updateFromFrontend : SessionId -> ClientId -> ToBackend -> Model -> ( Model, Cmd BackendMsg )
updateFromFrontend sessionId clientId msg model =
    case msg of
        ImportCsvContents csvContents ->
            let
                participants : Dict Int Participant
                participants =
                    Csv.Decode.decodeCsv Csv.Decode.FieldNamesFromFirstRow Participant.csvDecoder csvContents
                        |> Result.withDefault []
                        |> List.indexedMap Tuple.pair
                        |> Dict.fromList

                updatedParticipants : Dict Int Participant
                updatedParticipants =
                    Dict.union model.participants participants
            in
            ( { model | participants = updatedParticipants }
            , Lamdera.broadcast (GotParticipants updatedParticipants)
            )

        ResetParticipants ->
            ( { model | participants = Dict.empty }, Lamdera.broadcast (GotParticipants Dict.empty) )

        ChangeNiveau id niveau ->
            let
                updatedModel =
                    model |> setNiveauParticipant id niveau
            in
            ( updatedModel, Lamdera.broadcast (GotParticipants updatedModel.participants) )

        GenerateBandDistribution count method ->
            let
                bandsGenerator =
                    Band.createBands count method model.participants
            in
            ( model, Random.generate GotRandomBands bandsGenerator )


setNiveauParticipant : Int -> Niveau -> Model -> Model
setNiveauParticipant id niveau model =
    { model
        | participants =
            model.participants
                |> Dict.update
                    id
                    (Maybe.map (\participant -> { participant | niveau = niveau }))
    }
