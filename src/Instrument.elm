module Instrument exposing (Instrument(..), all, decoder, toAbbrev, toBadge, toString)

import Csv.Decode
import Html exposing (Html)
import Html.Attributes as Attr


type Instrument
    = Guitare
    | Violon
    | Harmonica
    | Chant
    | Flatfooting
    | Contrebasse
    | Dobro
    | Mandoline
    | BanjoBluegrass
    | BanjoOldtime


decoder : Csv.Decode.Decoder Instrument
decoder =
    Csv.Decode.string
        |> Csv.Decode.andThen
            (\string ->
                case string of
                    "guitare" ->
                        Csv.Decode.succeed Guitare

                    "violon" ->
                        Csv.Decode.succeed Violon

                    "harmonica" ->
                        Csv.Decode.succeed Harmonica

                    "chant" ->
                        Csv.Decode.succeed Chant

                    "flatfooting" ->
                        Csv.Decode.succeed Flatfooting

                    "contrebasse" ->
                        Csv.Decode.succeed Contrebasse

                    "dobro" ->
                        Csv.Decode.succeed Dobro

                    "mandoline" ->
                        Csv.Decode.succeed Mandoline

                    "banjo bluegrass" ->
                        Csv.Decode.succeed BanjoBluegrass

                    "banjo oldtime" ->
                        Csv.Decode.succeed BanjoOldtime

                    _ ->
                        Csv.Decode.fail <| "Instrument inconnu " ++ string
            )


toString : Instrument -> String
toString instrument =
    case instrument of
        Guitare ->
            "guitare"

        Violon ->
            "violon"

        Harmonica ->
            "harmonica"

        Chant ->
            "chant"

        Flatfooting ->
            "flatfooting"

        Contrebasse ->
            "contrebasse"

        Dobro ->
            "dobro"

        Mandoline ->
            "mandoline"

        BanjoBluegrass ->
            "banjo bluegrass"

        BanjoOldtime ->
            "banjo oldtime"


toColor : Instrument -> String
toColor instrument =
    case instrument of
        Guitare ->
            "#4B8FFF"

        Violon ->
            "#B39DDB"

        Harmonica ->
            "#DC3D8D"

        Chant ->
            "#FF8C7F"

        Flatfooting ->
            "#6666FF"

        Contrebasse ->
            "#8F00FF"

        Dobro ->
            "#FFD300"

        Mandoline ->
            "#00A0A0"

        BanjoBluegrass ->
            "#00BFFF"

        BanjoOldtime ->
            "#D2B4B4"


toBadge : Instrument -> Html msg
toBadge instrument =
    Html.span
        [ Attr.style "background-color" (toColor instrument)
        , Attr.style "color" "#000"
        , Attr.style "padding" ".2em .3em"
        , Attr.style "font-size" ".8em"
        , Attr.style "margin-right" ".5em"
        , Attr.style "border-radius" ".2em"
        ]
        [ Html.text (toString instrument) ]


toAbbrev : Instrument -> String
toAbbrev instrument =
    case instrument of
        Guitare ->
            "GT"

        Violon ->
            "VL"

        Harmonica ->
            "HM"

        Chant ->
            "CH"

        Flatfooting ->
            "FF"

        Contrebasse ->
            "CB"

        Dobro ->
            "DO"

        Mandoline ->
            "MD"

        BanjoBluegrass ->
            "BBG"

        BanjoOldtime ->
            "BOT"


all : List Instrument
all =
    [ Chant
    , Guitare
    , BanjoBluegrass
    , BanjoOldtime
    , Violon
    , Mandoline
    , Dobro
    , Harmonica
    , Contrebasse
    , Flatfooting
    ]
